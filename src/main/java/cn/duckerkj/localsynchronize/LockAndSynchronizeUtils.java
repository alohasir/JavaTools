package cn.duckerkj.localsynchronize;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

/**
 * Lock 显示锁
 * 	支持更小粒度====读写锁
 * Synchronize 内部锁
 * 1）Lock是一个接口，而synchronized是Java中的关键字，synchronized是内置的语言实现；

　　2）synchronized在发生异常时，会自动释放线程占有的锁，因此不会导致死锁现象发生；而Lock在发生异常时，如果没有主动通过unLock()去释放锁，则很可能造成死锁现象，因此使用Lock时需要在finally块中释放锁；

　　3）Lock可以让等待锁的线程响应中断，而synchronized却不行，使用synchronized时，等待的线程会一直等待下去，不能够响应中断；

　　4）通过Lock可以知道有没有成功获取锁，而synchronized却无法办到。

　　5）Lock可以提高多个线程进行读操作的效率。

   6）Lock 无阻塞    Sychronize阻塞
   	  Lock 公平锁    Sychronize非公平锁  等的时间久的先拿到  不一定完全这样

　　在性能上来说，如果竞争资源不激烈，两者的性能是差不多的，而当竞争资源非常激烈时（即有大量线程同时竞争），此时Lock的性能要远远优于synchronized。所以说，在具体使用时要根据适当情况选择。
 * 
 * 效果
 * 	pool-1-thread-1read.......14
	pool-1-thread-1write.......15
	pool-1-thread-4write.......16
	pool-1-thread-4write.......17
	pool-1-thread-3read.......17
	pool-1-thread-2read.......17
	pool-1-thread-4read.......17
	pool-1-thread-5read.......17
	pool-1-thread-1read.......17
 * @author Shensg
 * 2018年7月16日
 */
public class LockAndSynchronizeUtils {
	public static void main(String[] args) {
		/**
		 * 实现并发 一个写多个读 
		 */
		//模拟产生多线程,尝试5个线程池
		ExecutorService es = Executors.newFixedThreadPool(5);
		Foo foo = new Foo();
		//模拟并发
		for (int i = 0; i < 100; i++) {
			es.submit(new FooWriteRunnabel(foo));//写操作
			es.submit(new FooReadRunnabel(foo));//读操作
		}
		es.shutdown();
	}
	
	
}
