package cn.duckerkj.htmlautoversion;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;

/**
 * JavaScript工具类。
 * 
 * @author Michael J Chane
 * @version $Revision: 1.2 $ $Date: 2009/09/10 15:08:30 $
 */
public final class JavaScriptUtil {

  /**
   * 特殊字符
   */
  private static final String SPECIAL_CHARACTERS = "\n\r\f\'\"\\";

  /**
   * 特殊字符-转义字符的映射
   */
  private static final Properties ESCAPE_MAP = new Properties();
  static {
    ESCAPE_MAP.put("\n", "\\n");
    ESCAPE_MAP.put("\r", "\\r");
    ESCAPE_MAP.put("\f", "\\f");
    ESCAPE_MAP.put("\'", "\\\'");
    ESCAPE_MAP.put("\"", "\\\"");
    ESCAPE_MAP.put("\\", "\\\\");
  }

  /**
   * 不需要实例化
   */
  private JavaScriptUtil() {
  }

  /**
   * 对JavaScript字符串内部的文本进行转义操作，避免字符串内的特殊字符影响JavaScript代码的执行。
   * 
   * @param text
   *          要转义的字符串文本
   * @return 转义后的字符串文本
   */
  public static String escapeInStringLiterals(CharSequence text) {
    // 对null返回空白字符串
    if (text == null) {
      return "";
    }

    // 字符串缓冲
    StringBuilder buff = new StringBuilder(text.length() + 16);
    // 以特殊字符为分隔符，将文本分段（含特殊字符）
    StringTokenizer st = new StringTokenizer(text.toString(),
        SPECIAL_CHARACTERS, true);

    while (st.hasMoreTokens()) {
      // 当前片段
      String token = st.nextToken();
      // 如果是特殊字符则转义，否则返回本身
      buff.append(ESCAPE_MAP.getProperty(token, token));
    }

    return buff.toString();
  }

  /**
   * 将指定的JavaScript代码进行混淆。
   * 
   * @param script
   *          指定的JavaScript代码
   * @return 混淆后的JavaScript代码
   */
  public static String obfuscateScript(CharSequence script) {
    if (script == null) {
      return "";
    }

    // String.fromCharCode中参数最大个数
    final int stringFromCharCodeLimit = 100;
    // 每行的参数个数
    final int parametersPerLine = 10;
    // 使用xor函数的比例
    final float xorRate = 0.1f;
    // 字符缓冲
    StringBuilder buff = new StringBuilder(script.length() * 10 + 500);
    // 格式化输出到字符串缓冲
    Formatter formatter = new Formatter(buff);

    // 输出String.fromCharCode的别名定义，并返回其别名
    String stringFromCharCode = stringFromCharCode(formatter);
    // 输出xor函数，并返回函数名列表及对应的xor阈值
    Map<String, Integer> xorFunctions = xorFunctions(formatter);
    // xor函数名称
    String[] xorFuncNames = xorFunctions.keySet().toArray(new String[0]);

    // eval函数开始，其中第一个使用String.fromCharCode(32)即空格
    formatter.format("/*%2$s*/\\u0065\\u0076\\u0061\\u006c/*%3$s*/(%1$s(32",
                     formatArguments(3, stringFromCharCode));

    // 遍历代码中的所有字符
    for (int i = 0; i < script.length(); i++) {
      // 当前字符
      int code = script.charAt(i);

      if (i % stringFromCharCodeLimit == 0) {
        // 结束旧的String.fromCharCode，
        formatter.format(")%n");
        // 开始新的String.fromCharCode
        formatter.format("+/*%2$s*/%1$s(", formatArguments(2,
                                                           stringFromCharCode));
      } else {
        // 一般的String.fromCharCode参数之间使用逗号分隔
        buff.append(",");
        if (i % parametersPerLine == 0) {
          // 当前字符结束后需要换行
          formatter.format("%n");
        }
      }

      // 根据xorRate确定的比例，输出当前字符参数
      if (RandomUtils.nextFloat() < xorRate) {
        // 使用xor参数的名称
        String xorFunc = xorFuncNames[i % xorFuncNames.length];
        // 对应的异或计算阈值
        int xor = xorFunctions.get(xorFunc);
        // 进行过异或计算后的结果
        int xorCode = code ^ xor;
        // 输出函数调用
        formatter.format("%1$s(", xorFunc);
        // 输出函数参数
        formatter.format(numberFormat(i), xorCode);
        // 调用结束
        buff.append(")");
      } else {
        // 正常输出
        formatter.format(numberFormat(i), code);
      }
    }

    // 最后一个String.fromCharCode和eval函数的结尾
    formatter.format("/*%1$s%2$s*/));/*%3$s*/%n", formatArguments(3));

    // 返回混淆代码
    return buff.toString();
  }

  /**
   * 输出String.fromCharCode的别名定义，并返回其别名。
   * 
   * @param formatter
   *          格式化输出
   * @return String.fromCharCode别名
   */
  private static String stringFromCharCode(Formatter formatter) {
    String stringFromCharCode = "__" + randomAlphanumeric(3, 10);
    formatter
        .format("/*%2$s*/var/*%3$s*/%1$s/*%4$s*/=\\u0053\\u0074\\u0072\\u0069\\u006e\\u0067%n/*%5$s*/./*%6$s*/\\u0066r\\u006fm\\u0043ha\\u0072C\\u006fde/*%7$s*/;%n",
                formatArguments(7, stringFromCharCode));
    return stringFromCharCode;
  }

  /**
   * 输出xor函数，并返回函数名列表及对应的xor阈值。
   * 
   * @param formatter
   *          格式化输出
   * @return xor函数名列表及对应的xor阈值
   */
  private static Map<String, Integer> xorFunctions(Formatter formatter) {
    int[] xorArray = new int[5];
    for (int i = 0; i < xorArray.length; i++) {
      xorArray[i] = RandomUtils.nextInt(4096);
    }
    String xorArrayName = "_x_" + randomAlphanumeric(3);
    formatter.format("var/*%2$s*/%1$s = [/*%3$s*/",
                     formatArguments(3, xorArrayName));
    for (int i = 0; i < xorArray.length; i++) {
      formatter.format("%d,", xorArray[i]);
    }
    formatter.format("/*%s*/];//%s%n", formatArguments(2));

    Map<String, Integer> functions = new HashMap<String, Integer>();
    for (int i = 0; i < xorArray.length; i++) {
      String func = "_$" + randomAlphanumeric(3, 5);
      formatter.format("var/*%2$s*/%1$s/*%3$s*/=/*%4$s*/function(/*%5$s*/){%n",
                       formatArguments(5, func));
      formatter.format("/*%1$s*/return/*%2$s*/arguments[/*%3$s*/0]^/*%4$s*/%n",
                       formatArguments(4));
      formatter.format("/*%3$s*/%1$s[/*%4$s*/%2$d];/*%5$s*/}/*%6$s*/;%n",
                       formatArguments(6, xorArrayName, i));
      functions.put(func, xorArray[i]);
    }
    return functions;
  }

  /**
   * 获取格式化输出参数。
   * 
   * @param count
   *          参数个数
   * @param firstFixedParameters
   *          最初的固定参数
   * @return 根据给定的参数个数，在最初的固定参数后生成随机字符串作为格式化输出参数
   */
  private static Object[] formatArguments(int count,
                                          Object... firstFixedParameters) {
    if (count < firstFixedParameters.length) {
      throw new IllegalArgumentException("length < codes.length");
    }
    Object[] args = new Object[count];
    System.arraycopy(firstFixedParameters,
                     0,
                     args,
                     0,
                     firstFixedParameters.length);
    for (int i = firstFixedParameters.length; i < args.length; i++) {
      args[i] = randomAlphanumeric(5, 20);
    }
    return args;
  }

  /**
   * 生成由字母及数字组成的随机字符串。
   * 
   * @param length
   *          随机字符串长度
   * @return 由字母及数字组成的随机字符串
   */
  private static String randomAlphanumeric(int length) {
    return randomAlphanumeric(length, length);
  }

  /**
   * 生成由字母及数字组成的随机字符串。
   * 
   * @param minLength
   *          随机字符串最小长度
   * @param maxLength
   *          随机字符串最大长度
   * @return 由字母及数字组成的随机字符串
   */
  private static String randomAlphanumeric(int minLength, int maxLength) {
    if (minLength <= 0) {
      throw new IllegalArgumentException("minLength <= 0");
    }
    if (maxLength <= 0) {
      throw new IllegalArgumentException("maxLength <= 0");
    }
    if (minLength > maxLength) {
      throw new IllegalArgumentException("minLength > maxLength");
    } else if (minLength == maxLength) {
      return RandomStringUtils.randomAlphanumeric(minLength);
    } else {
      int length = minLength + RandomUtils.nextInt(maxLength - minLength);
      return RandomStringUtils.randomAlphanumeric(length);
    }
  }

  /**
   * 随机的格式化输出格式。
   * 
   * @param seed
   *          随机种子
   * @return 随机的格式化输出格式
   */
  private static String numberFormat(int seed) {
    int rnd = RandomUtils.nextInt(Math.abs(seed) + 100);
    switch (rnd % 17) {
      case 0:
        return "0x%x";
      case 1:
        return String.format("-1-~/*%s*/(0x%%x^0)", randomAlphanumeric(2, 5));
      case 2:
        return String.format("%%d%d/0xA", RandomUtils.nextInt(10));
      case 3:
        return "Math.abs(%d)&-1";
      case 4:
        return "0%o";
      case 5:
        return "%d&(-1^0x00)";
      case 6:
        return "0x0|0x%x";
      case 7:
        return String.format("~/*%s*/~/*%s*/%%d", formatArguments(2));
      case 8:
        return String.format("~(0x%%x^/*%s*/-1)", randomAlphanumeric(2, 5));
      case 9:
        return String.format("0x%%x%d%d/0400",
                             RandomUtils.nextInt(10),
                             RandomUtils.nextInt(10));
      case 10:
        return String.format("0x%%x%d%d>>/*%s*/4>>4",
                             formatArguments(3,
                                             RandomUtils.nextInt(10),
                                             RandomUtils.nextInt(10)));
      case 11:
        return String.format("%%d/*%s*/", randomAlphanumeric(2));
      default:
        return "%d";
    }
  }
  
  public static void main(String[] args) {
	  System.out.println(obfuscateScript("\r\n" + 
	  		"/*地址栏获取参数*/\r\n" + 
	  		"function getQueryString(name) { \r\n" + 
	  		"    var reg = new RegExp(\"(^|&)\" + name + \"=([^&]*)(&|$)\", \"i\"); \r\n" + 
	  		"    var r = decodeURI(window.location.search).substr(1).match(reg); \r\n" + 
	  		"    if (r != null) return unescape(r[2]); return null; \r\n" + 
	  		"} \r\n" + 
	  		"/*地址栏获取参数*/\r\n" + 
	  		"function getMyUrlQueryString(url,name) { \r\n" + 
	  		"    var reg = new RegExp(\"[^\\?&]?\" + encodeURI(name) + \"=[^&]+\");\r\n" + 
	  		"    var arr = url.match(reg);\r\n" + 
	  		"    if (arr != null) {\r\n" + 
	  		"        return decodeURI(arr[0].substring(arr[0].search(\"=\") + 1));\r\n" + 
	  		"    }\r\n" + 
	  		"    return \"\";\r\n" + 
	  		"} \r\n" + 
	  		"\r\n" + 
	  		"/*本地存储数据*/\r\n" + 
	  		"function setLocalInfo(name,data){\r\n" + 
	  		"     if (window.applicationCache&&window.localStorage) {\r\n" + 
	  		"         //console.log(\"你的浏览器支持HTML5\");\r\n" + 
	  		"         localStorage.setItem(name,data);\r\n" + 
	  		"     } else {\r\n" + 
	  		"         //console.log(\"你的浏览器不支持HTML5 采用cookie存储\");\r\n" + 
	  		"         $.cookie(name, data, { expires: 7 }); ;\r\n" + 
	  		"     }\r\n" + 
	  		"}\r\n" + 
	  		"\r\n" + 
	  		"/*本地存储数据 获取*/\r\n" + 
	  		"function getLocalInfo(name){\r\n" + 
	  		"     if (window.applicationCache&&window.localStorage) {\r\n" + 
	  		"        // console.log(\"你的浏览器支持HTML5\");\r\n" + 
	  		"         return localStorage.getItem(name);\r\n" + 
	  		"     } else {\r\n" + 
	  		"         //console.log(\"你的浏览器不支持HTML5 采用cookie存储\");\r\n" + 
	  		"         return $.cookie(name);\r\n" + 
	  		"     }\r\n" + 
	  		"}\r\n" + 
	  		"\r\n" + 
	  		"/*本地存储数据 删除*/\r\n" + 
	  		"function removeLocalInfo(name){\r\n" + 
	  		"     if (window.applicationCache&&window.localStorage) {\r\n" + 
	  		"         //console.log(\"你的浏览器支持HTML5\");\r\n" + 
	  		"         return localStorage.removeItem(name);\r\n" + 
	  		"     } else {\r\n" + 
	  		"         //console.log(\"你的浏览器不支持HTML5 采用cookie存储\");\r\n" + 
	  		"         return $.cookie(name,null);\r\n" + 
	  		"     }\r\n" + 
	  		"}\r\n" + 
	  		"/*全局捕获异常信息*/\r\n" + 
	  		"onerror=handleErr\r\n" + 
	  		"\r\n" + 
	  		"function handleErr(msg,url,l)\r\n" + 
	  		"{ \r\n" + 
	  		"    var txt=\"\";\r\n" + 
	  		"    txt+=\"Error: <span style='color:red' >\" + msg + \" </span> <br/>\"\r\n" + 
	  		"    txt+=\"URL:   <span style='color:blue' >\" + url + \" </span> <br/>\"\r\n" + 
	  		"    txt+=\"Line:  <span style='color:green' >\" + l   + \" </span> <br/>\"\r\n" + 
	  		"    txt+=\".\\n\\n\"\r\n" + 
	  		"    if(filterError(msg,url,l))show_error_dialog(txt);\r\n" + 
	  		"    return false;\r\n" + 
	  		"}\r\n" + 
	  		"/*过滤警告*/\r\n" + 
	  		"function filterError(msg,url,line_msg){\r\n" + 
	  		"    if(msg.indexOf(\"isDefaultPrevented\")>0&&url.indexOf(\"H-ui.js\")>0&&line_msg==5402){\r\n" + 
	  		"        return false;\r\n" + 
	  		"    }\r\n" + 
	  		"    if(msg.indexOf(\"hide\")>0&&url.indexOf(\"contextMenu\")>0&&line_msg==367){\r\n" + 
	  		"        return false;\r\n" + 
	  		"    }\r\n" + 
	  		"    if(msg.indexOf(\"text\")>0&&url.indexOf(\"tb-order-info\")>0&&line_msg==7){\r\n" + 
	  		"    	window.location.reload();\r\n" + 
	  		"        return false;\r\n" + 
	  		"    }\r\n" + 
	  		"    if(msg.indexOf(\"length\")>-1&&url.indexOf(\"my.form\")>-1){\r\n" + 
	  		"    	$(\"[name^='order_']\").each(function(i,elem){\r\n" + 
	  		"    		if($(elem)[0].tagName==\"SELECT\"){\r\n" + 
	  		"    			if($(elem)[0].options.length<1){\r\n" + 
	  		"    				var text = $(this).parent().parent().find(\".right-div-comp-left-text-span\").text();\r\n" + 
	  		"    				alert(text+'下拉菜单请选择数据');\r\n" + 
	  		"    			}\r\n" + 
	  		"    		}\r\n" + 
	  		"    	})\r\n" + 
	  		"    	$.messager.progress('close');\r\n" + 
	  		"    	return false;\r\n" + 
	  		"    }\r\n" + 
	  		"    return true;\r\n" + 
	  		"}\r\n" + 
	  		"/*展示错误信息*/\r\n" + 
	  		"function show_error_dialog(msg){\r\n" + 
	  		"    $('#dialogforsysinfo').dialogBox({\r\n" + 
	  		"            type: 'error',  //three type:'normal'(default),'correct','error',\r\n" + 
	  		"            width: 300,\r\n" + 
	  		"            height: 300,\r\n" + 
	  		"            hasMask: true,\r\n" + 
	  		"            //autoHide: true,\r\n" + 
	  		"            hasClose:true,\r\n" + 
	  		"            //time: 3000,\r\n" + 
	  		"            effect: 'fall',//fall\r\n" + 
	  		"            title: '错误提醒',\r\n" + 
	  		"            content: msg\r\n" + 
	  		"        });\r\n" + 
	  		"}\r\n" + 
	  		"\r\n" + 
	  		"function toUtf8(str) {\r\n" + 
	  		"    var out, i, len, c;\r\n" + 
	  		"    out = \"\";\r\n" + 
	  		"    len = str.length;\r\n" + 
	  		"    for(i = 0; i < len; i++) {\r\n" + 
	  		"        c = str.charCodeAt(i);\r\n" + 
	  		"        if ((c >= 0x0001) && (c <= 0x007F)) {\r\n" + 
	  		"            out += str.charAt(i);\r\n" + 
	  		"        } else if (c > 0x07FF) {\r\n" + 
	  		"            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));\r\n" + 
	  		"            out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));\r\n" + 
	  		"            out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));\r\n" + 
	  		"        } else {\r\n" + 
	  		"            out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));\r\n" + 
	  		"            out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));\r\n" + 
	  		"        }\r\n" + 
	  		"    }\r\n" + 
	  		"    return out;\r\n" + 
	  		"}\r\n" + 
	  		"\r\n" + 
	  		"function newWinNoRepeat(url,num) {  \r\n" + 
	  		"     /* var a = document.createElement('a');  \r\n" + 
	  		"      a.setAttribute('href', url);  \r\n" + 
	  		"      a.setAttribute('target', 'win'+num);  \r\n" + 
	  		"      a.setAttribute('id', 'newWinNoRepeat');  \r\n" + 
	  		"      // 防止反复添加\r\n" + 
	  		"      if(!document.getElementById('newWinNoRepeat')) {                       \r\n" + 
	  		"          document.body.appendChild(a);  \r\n" + 
	  		"      }  \r\n" + 
	  		"      a.click();  */\r\n" + 
	  		"	var time = 3;\r\n" + 
	  		"	$('#dialogforsysinfo').dialogBox({\r\n" + 
	  		"		type : 'normal', // three type:'normal'(default),'correct','error',\r\n" + 
	  		"		hasMask : true,\r\n" + 
	  		"		hasClose : true,\r\n" + 
	  		"		effect : 'fall',// fall\r\n" + 
	  		"		title : '页面跳转提醒',\r\n" + 
	  		"		content : \"<span id='timesgodown'>\"+time+\"</span>s 页面即将刷新,请稍等...\"\r\n" + 
	  		"	});\r\n" + 
	  		"	setTime = setInterval(function() {\r\n" + 
	  		"		if (time <= 0) {\r\n" + 
	  		"			clearInterval(setTime);\r\n" + 
	  		"			window.location.href = url;\r\n" + 
	  		"			return;\r\n" + 
	  		"		}\r\n" + 
	  		"		time--;\r\n" + 
	  		"		$(\"#timesgodown\").text(time);\r\n" + 
	  		"	}, 1000);\r\n" + 
	  		"	// window.location.href=url;\r\n" + 
	  		"} \r\n" + 
	  		"// 中文标点转为英文\r\n" + 
	  		"function qj2bj(str){  \r\n" + 
	  		"    var tmp = \"\";  \r\n" + 
	  		"    for(var i=0;i<str.length;i++){  \r\n" + 
	  		"        if(str.charCodeAt(i) >= 65281 && str.charCodeAt(i) <= 65374){// 如果位于全角！到全角～区间内\r\n" + 
	  		"            tmp += String.fromCharCode(str.charCodeAt(i)-65248)  \r\n" + 
	  		"        }else if(str.charCodeAt(i) == 12288){//全角空格的值，它没有遵从与ASCII的相对偏移，必须单独处理\r\n" + 
	  		"            tmp += ' ';  \r\n" + 
	  		"        }else{// 不处理全角空格，全角！到全角～区间外的字符\r\n" + 
	  		"            tmp += str[i];  \r\n" + 
	  		"        }  \r\n" + 
	  		"    }  \r\n" + 
	  		"    return tmp;  \r\n" + 
	  		"}  \r\n" + 
	  		"\r\n" + 
	  		"var lastUniqueId;\r\n" + 
	  		"var getuniqueId_index=0;\r\n" + 
	  		"function getuniqueId(key){\r\n" + 
	  		"	var uniqueId\r\n" + 
	  		"	while(true){\r\n" + 
	  		"		getuniqueId_index++;\r\n" + 
	  		"		uniqueId = Number(Math.random().toString().substr(3,length) + Date.now()).toString(36)+getuniqueId_index;\r\n" + 
	  		"		if(uniqueId==lastUniqueId){//避免并发同时获取出现不唯一\r\n" + 
	  		"			continue;\r\n" + 
	  		"		}\r\n" + 
	  		"		if($(\"[\"+key+\"='\"+uniqueId+\"']\").length==0)break;\r\n" + 
	  		"	}\r\n" + 
	  		"	lastUniqueId=uniqueId;\r\n" + 
	  		"	\r\n" + 
	  		"	return uniqueId;\r\n" + 
	  		"}\r\n" + 
	  		"//获取时间格式化\r\n" + 
	  		"function utilgetFormatDate(fmt){    \r\n" + 
	  		"    var nowDate = new Date();     \r\n" + 
	  		"    var year = nowDate.getFullYear();    \r\n" + 
	  		"    var month = nowDate.getMonth() + 1 < 10 ? \"0\" + (nowDate.getMonth() + 1) : nowDate.getMonth() + 1;    \r\n" + 
	  		"    var date = nowDate.getDate() < 10 ? \"0\" + nowDate.getDate() : nowDate.getDate();    \r\n" + 
	  		"    var hour = nowDate.getHours()< 10 ? \"0\" + nowDate.getHours() : nowDate.getHours();    \r\n" + 
	  		"    var minute = nowDate.getMinutes()< 10 ? \"0\" + nowDate.getMinutes() : nowDate.getMinutes();    \r\n" + 
	  		"    var second = nowDate.getSeconds()< 10 ? \"0\" + nowDate.getSeconds() : nowDate.getSeconds();    \r\n" + 
	  		"    if(fmt==\"yyyy-MM-dd\"){\r\n" + 
	  		"    	return year + \"-\" + month + \"-\" + date;   \r\n" + 
	  		"    }\r\n" + 
	  		"    if(fmt==\"yyyy-MM-dd HH:mm:SS\"){\r\n" + 
	  		"    	return year + \"-\" + month + \"-\" + date+\" \"+hour+\":\"+minute+\":\"+second;  \r\n" + 
	  		"    }\r\n" + 
	  		"}\r\n" + 
	  		"//时间大小比对\r\n" + 
	  		"function utilcheckDate(firsttime,secondtime) {\r\n" + 
	  		"	firsttime = new Date(firsttime.replace(\"-\", \"/\").replace(\"-\", \"/\"));\r\n" + 
	  		"	secondtime = new Date(secondtime.replace(\"-\", \"/\").replace(\"-\", \"/\"));\r\n" + 
	  		"	if (firsttime >= secondtime) {\r\n" + 
	  		"		return true;\r\n" + 
	  		"	}else {\r\n" + 
	  		"		return false;\r\n" + 
	  		"	}\r\n" + 
	  		"}\r\n" + 
	  		"/**\r\n" + 
	  		" * 鼠标坐标位置\r\n" + 
	  		" * @param ev\r\n" + 
	  		" * @returns\r\n" + 
	  		" */\r\n" + 
	  		"var _mouse_x=0;\r\n" + 
	  		"var _mouse_y=0;\r\n" + 
	  		"function mouseMove(ev) {\r\n" + 
	  		"	Ev = ev || window.event;\r\n" + 
	  		"	var mousePos = mouseCoords(ev);\r\n" + 
	  		"	_mouse_x=mousePos.x;\r\n" + 
	  		"	_mouse_y=mousePos.y;\r\n" + 
	  		"}\r\n" + 
	  		"function mouseCoords(ev) {\r\n" + 
	  		"	if (ev.pageX || ev.pageY) {\r\n" + 
	  		"		return {\r\n" + 
	  		"			x : ev.pageX,\r\n" + 
	  		"			y : ev.pageY\r\n" + 
	  		"		};\r\n" + 
	  		"	}\r\n" + 
	  		"	return {\r\n" + 
	  		"		x : ev.clientX + document.body.scrollLeft - document.body.clientLeft,\r\n" + 
	  		"		y : ev.clientY + document.body.scrollTop - document.body.clientTop\r\n" + 
	  		"	};\r\n" + 
	  		"} \r\n" + 
	  		"document.onmousemove = mouseMove; \r\n" + 
	  		"\r\n" + 
	  		"(function($, h, c) {\r\n" + 
	  		"	var a = $([]),\r\n" + 
	  		"	e = $.resize = $.extend($.resize, {}),\r\n" + 
	  		"	i,\r\n" + 
	  		"	k = \"setTimeout\",\r\n" + 
	  		"	j = \"resize\",\r\n" + 
	  		"	d = j + \"-special-event\",\r\n" + 
	  		"	b = \"delay\",\r\n" + 
	  		"	f = \"throttleWindow\";\r\n" + 
	  		"	e[b] = 250;\r\n" + 
	  		"	e[f] = true;\r\n" + 
	  		"	$.event.special[j] = {\r\n" + 
	  		"		setup: function() {\r\n" + 
	  		"			if (!e[f] && this[k]) {\r\n" + 
	  		"				return false;\r\n" + 
	  		"			}\r\n" + 
	  		"			var l = $(this);\r\n" + 
	  		"			a = a.add(l);\r\n" + 
	  		"			$.data(this, d, {\r\n" + 
	  		"				w: l.width(),\r\n" + 
	  		"				h: l.height()\r\n" + 
	  		"			});\r\n" + 
	  		"			if (a.length === 1) {\r\n" + 
	  		"				g();\r\n" + 
	  		"			}\r\n" + 
	  		"		},\r\n" + 
	  		"		teardown: function() {\r\n" + 
	  		"			if (!e[f] && this[k]) {\r\n" + 
	  		"				return false;\r\n" + 
	  		"			}\r\n" + 
	  		"			var l = $(this);\r\n" + 
	  		"			a = a.not(l);\r\n" + 
	  		"			l.removeData(d);\r\n" + 
	  		"			if (!a.length) {\r\n" + 
	  		"				clearTimeout(i);\r\n" + 
	  		"			}\r\n" + 
	  		"		},\r\n" + 
	  		"		add: function(l) {\r\n" + 
	  		"			if (!e[f] && this[k]) {\r\n" + 
	  		"				return false;\r\n" + 
	  		"			}\r\n" + 
	  		"			var n;\r\n" + 
	  		"			function m(s, o, p) {\r\n" + 
	  		"				var q = $(this),\r\n" + 
	  		"				r = $.data(this, d);\r\n" + 
	  		"				r.w = o !== c ? o: q.width();\r\n" + 
	  		"				r.h = p !== c ? p: q.height();\r\n" + 
	  		"				n.apply(this, arguments);\r\n" + 
	  		"			}\r\n" + 
	  		"			if ($.isFunction(l)) {\r\n" + 
	  		"				n = l;\r\n" + 
	  		"				return m;\r\n" + 
	  		"			} else {\r\n" + 
	  		"				n = l.handler;\r\n" + 
	  		"				l.handler = m;\r\n" + 
	  		"			}\r\n" + 
	  		"		}\r\n" + 
	  		"	};\r\n" + 
	  		"	function g() {\r\n" + 
	  		"		i = h[k](function() {\r\n" + 
	  		"			a.each(function() {\r\n" + 
	  		"				var n = $(this),\r\n" + 
	  		"				m = n.width(),\r\n" + 
	  		"				l = n.height(),\r\n" + 
	  		"				o = $.data(this, d);\r\n" + 
	  		"				if (m !== o.w || l !== o.h) {\r\n" + 
	  		"					n.trigger(j, [o.w = m, o.h = l]);\r\n" + 
	  		"				}\r\n" + 
	  		"			});\r\n" + 
	  		"			g();\r\n" + 
	  		"		},\r\n" + 
	  		"		e[b]);\r\n" + 
	  		"	}\r\n" + 
	  		"})(jQuery, this);\r\n" + 
	  		"\r\n" + 
	  		"\r\n" + 
	  		"var UtilForEveryAreaEvent={\r\n" + 
	  		"		//订单数 投料数焦点事件\r\n" + 
	  		"	    orderTotalFeedTotal_blur:function(){\r\n" + 
	  		"	       $(\"[name='order_s_order_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_s_order_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).text(parseInt($(this).text().replace(/[^\\d]/g,'')));\r\n" + 
	  		"			   if(isNaN($(this).text())){//非数字为  1 \r\n" + 
	  		"				   $(this).text(1);\r\n" + 
	  		"				   //alert(\"注意：类型为数字\")\r\n" + 
	  		"				   return;\r\n" + 
	  		"			   }\r\n" + 
	  		"			   if(!isNaN($(this).text())){ //数字最小值 1\r\n" + 
	  		"				   if($(this).text()<1){\r\n" + 
	  		"					   //alert(\"注意：最小值默认为 0 \")\r\n" + 
	  		"					   $(this).text(0);\r\n" + 
	  		"				   }\r\n" + 
	  		"				   return;\r\n" + 
	  		"			   }\r\n" + 
	  		"		   })\r\n" + 
	  		"		   $(\"[name='order_s_feed_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_s_feed_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).text(parseInt($(this).text().replace(/[^\\d]/g,'')));\r\n" + 
	  		"			   if(isNaN($(this).text())){//非数字为  1 \r\n" + 
	  		"				   $(this).text(1);\r\n" + 
	  		"				   //alert(\"注意：类型为数字\")\r\n" + 
	  		"			   }\r\n" + 
	  		"			   if(!isNaN($(this).text())){ //数字最小值 1\r\n" + 
	  		"				   if($(this).text()<1){\r\n" + 
	  		"					   //alert(\"注意：最小值默认为 0 \")\r\n" + 
	  		"					   $(this).text(0);\r\n" + 
	  		"				   }\r\n" + 
	  		"			   }\r\n" + 
	  		"			   tbsourcefactoryColresizeEvent.feedTotalBlur();\r\n" + 
	  		"		   })\r\n" + 
	  		"		   $(\"[name='order_s_small_order_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_s_small_order_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).text(parseInt($(this).text().replace(/[^\\d]/g,'')));\r\n" + 
	  		"			   if(isNaN($(this).text())){//非数字为  1 \r\n" + 
	  		"				   $(this).text(1);\r\n" + 
	  		"				   //alert(\"注意：类型为数字\")\r\n" + 
	  		"				   return;\r\n" + 
	  		"			   }\r\n" + 
	  		"			   if(!isNaN($(this).text())){ //数字最小值 1\r\n" + 
	  		"				   if($(this).text()<1){\r\n" + 
	  		"					   //alert(\"注意：最小值默认为 0 \")\r\n" + 
	  		"					   $(this).text(0);\r\n" + 
	  		"				   }\r\n" + 
	  		"				   return;\r\n" + 
	  		"			   }\r\n" + 
	  		"		   })\r\n" + 
	  		"		   $(\"[name='order_s_small_feed_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_s_small_feed_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).text(parseInt($(this).text().replace(/[^\\d]/g,'')));\r\n" + 
	  		"			   if(isNaN($(this).text())){//非数字为  1 \r\n" + 
	  		"				   $(this).text(1);\r\n" + 
	  		"				   //alert(\"注意：类型为数字\")\r\n" + 
	  		"			   }\r\n" + 
	  		"			   if(!isNaN($(this).text())){ //数字最小值 1\r\n" + 
	  		"				   if($(this).text()<1){\r\n" + 
	  		"					   //alert(\"注意：最小值默认为 0 \")\r\n" + 
	  		"					   $(this).text(0);\r\n" + 
	  		"				   }\r\n" + 
	  		"			   }\r\n" + 
	  		"			   tbsourcefactoryColresizeEvent.feedTotalBlur();\r\n" + 
	  		"		   })\r\n" + 
	  		"		   $(\"[name='order_p_order_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_p_order_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).val($(this).val().replace(/[^\\d]/g,''));\r\n" + 
	  		"		   })\r\n" + 
	  		"		   $(\"[name='order_p_feed_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_p_feed_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).val($(this).val().replace(/[^\\d]/g,''));\r\n" + 
	  		"		   })\r\n" + 
	  		"		   //样式5投料数\r\n" + 
	  		"		   $(\"[name='order_s_input_val_feed_total']\").unbind(\"blur\");\r\n" + 
	  		"		   $(\"[name='order_s_input_val_feed_total']\").bind(\"blur\",function(){\r\n" + 
	  		"			   $(this).val($(this).val().replace(/[^\\d]/g,''));\r\n" + 
	  		"		   })\r\n" + 
	  		"	    },\r\n" + 
	  		"	    //allName ',' 所有name不能写入,号\r\n" + 
	  		"	    allNameReplaceDH:function(){\r\n" + 
	  		"	    	console.info(\"setInterval UtilForEveryAreaEvent.allNameReplaceDH\");\r\n" + 
	  		"	    	 $(\"#myformsubmit\").find(\"[name^='order_']\").each(function(i,elem){\r\n" + 
	  		"	    		 if(($(elem)[0].tagName==\"DIV\"||$(elem)[0].tagName==\"TD\"||$(elem)[0].tagName==\"TEXTAREA\")&&($(elem).attr(\"name\").indexOf(\"order_p\")<0)){\r\n" + 
	  		"	    			//所有不允许输入~\r\n" + 
	  		"	 	 	    	if($(this).text()!=undefined){\r\n" + 
	  		"	 	 	    		var _text = $(this).text();\r\n" + 
	  		"	 	 	    		_text=qj2bj(_text);\r\n" + 
	  		"	 	 	    		while(_text.indexOf(\"~\")>-1){\r\n" + 
	  		"	 	 	    			_text=_text.replace(\"~\",\" \");\r\n" + 
	  		"	 	 	    			$(this).text(_text);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	 	 	    	if($(this).val()!=undefined){\r\n" + 
	  		"	 	 	    		var _val = $(this).val();\r\n" + 
	  		"	 	 	    		_val=qj2bj(_val);\r\n" + 
	  		"	 	 	    		while(_val.indexOf(\"~\")>-1){\r\n" + 
	  		"	 	 	    			_val=_val.replace(\"~\",\" \");\r\n" + 
	  		"	 	 	    			$(this).val(_val);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	 	 	    	//判断逗号和无\r\n" + 
	  		"	    			 if($(elem).attr(\"name\").indexOf(\"order_total\")>-1||$(elem).attr(\"name\").indexOf(\"feed_total\")>-1\r\n" + 
	  		"	    					 ||$(elem).attr(\"name\").indexOf(\"order_label_table\")>-1\r\n" + 
	  		"	    					 ||$(elem).attr(\"name\").indexOf(\"order_label_label\")>-1\r\n" + 
	  		"	    					 ||$(elem).attr(\"name\").indexOf(\"order_p_cust_remark\")>-1){//排除自定义标签和表格和客户要求\r\n" + 
	  		"	    				 return;\r\n" + 
	  		"	    			 }\r\n" + 
	  		"	    			 if($(this).text()!=undefined){\r\n" + 
	  		"	 	 	    		var _text = $(this).text();\r\n" + 
	  		"	 	 	    		_text=qj2bj(_text);\r\n" + 
	  		"	 	 	    		while(_text.indexOf(\",\")>-1){\r\n" + 
	  		"	 	 	    			_text=_text.replace(\",\",\" \");\r\n" + 
	  		"	 	 	    			$(this).text(_text);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	 	 	    	if($(this).val()!=undefined){\r\n" + 
	  		"	 	 	    		var _val = $(this).val();\r\n" + 
	  		"	 	 	    		_val=qj2bj(_val);\r\n" + 
	  		"	 	 	    		while(_val.indexOf(\",\")>-1){\r\n" + 
	  		"	 	 	    			_val=_val.replace(\",\",\" \");\r\n" + 
	  		"	 	 	    			$(this).val(_val);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	 	 	    	\r\n" + 
	  		"	 	 	    	//20180820去除无\r\n" + 
	  		"	 	 	    	if($(this).text()!=undefined){\r\n" + 
	  		"	 	 	    		var _text = $(this).text();\r\n" + 
	  		"	 	 	    		_text=qj2bj(_text);\r\n" + 
	  		"	 	 	    		while(_text.indexOf(\"无\")>-1&&_text.length==1){\r\n" + 
	  		"	 	 	    			_text=_text.replace(\"无\",\"\");\r\n" + 
	  		"	 	 	    			$(this).text(_text);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	 	 	    	if($(this).val()!=undefined){\r\n" + 
	  		"	 	 	    		var _val = $(this).val();\r\n" + 
	  		"	 	 	    		_val=qj2bj(_val);\r\n" + 
	  		"	 	 	    		while(_val.indexOf(\"无\")>-1&&_text.length==1){\r\n" + 
	  		"	 	 	    			_val=_val.replace(\"无\",\"\");\r\n" + 
	  		"	 	 	    			$(this).val(_val);\r\n" + 
	  		"	 	 	    		}\r\n" + 
	  		"	 	 	    	}\r\n" + 
	  		"	    		 }\r\n" + 
	  		"	    	 });\r\n" + 
	  		"	    }\r\n" + 
	  		"}\r\n" + 
	  		"var Interval_UtilForEveryAreaEvent_allNameReplaceDH = setInterval(function(){UtilForEveryAreaEvent.allNameReplaceDH()},2000);\r\n" + 
	  		"/**\r\n" + 
	  		" *移动组件\r\n" + 
	  		" */\r\n" + 
	  		"var doc_mydraggablehandler = $(document);\r\n" + 
	  		"function mydraggablehandler(){\r\n" + 
	  		"	$(\"div.mydraggablehandler\").unbind(\"mousedown\");\r\n" + 
	  		"    $(\"div.mydraggablehandler\").bind(\"mousedown\",function(e) {\r\n" + 
	  		"    	doc_mydraggablehandler = $(this).parent().parent().parent().parent();\r\n" + 
	  		"    	var dl = $(this).parent(), dc = \"\";\r\n" + 
	  		"    	/*if($(this).parent().next().attr(\"class\")=='right-div-comp-right-content-for-five'){\r\n" + 
	  		"    		dc = $(this).parent().nextAll().eq(3);\r\n" + 
	  		"    	}*/\r\n" + 
	  		"    	/*if($(this).parent().attr(\"class\")=='right-div-comp-left-text'){\r\n" + 
	  		"    		dc = $(this).parent().parent().find(\".right-div-comp-right-content\");\r\n" + 
	  		"    	}*/\r\n" + 
	  		"    	//投料数点击 \r\n" + 
	  		"    	if($(this).parent().attr(\"class\")=='right-div-comp-right-content-for-five'){\r\n" + 
	  		"    		//工艺隐藏并且材质显示 \r\n" + 
	  		"    		if($(this).parents().find(\".right-div-comp-right-content-for-six\").css(\"display\")==\"none\"&&$(this).parents().find(\".right-div-comp-right-content-for-seven\").css(\"display\")!=\"none\"){\r\n" + 
	  		"    			dc = $(this).parent().parent().find(\".right-div-comp-right-content-for-seven\");\r\n" + 
	  		"    		}\r\n" + 
	  		"    		//材质隐藏并且工艺显示 \r\n" + 
	  		"    		if($(this).parents().find(\".right-div-comp-right-content-for-seven\").css(\"display\")==\"none\"&&$(this).parents().find(\".right-div-comp-right-content-for-six\").css(\"display\")!=\"none\"){\r\n" + 
	  		"    			dc = $(this).parent().parent().find(\".right-div-comp-right-content-for-six\");\r\n" + 
	  		"    		}\r\n" + 
	  		"    		//工艺隐藏并且材质隐藏 则直接是物料类型\r\n" + 
	  		"    		if($(this).parents().find(\".right-div-comp-right-content-for-seven\").css(\"display\")==\"none\"&&$(this).parents().find(\".right-div-comp-right-content-for-six\").css(\"display\")==\"none\"){\r\n" + 
	  		"    			dc = $(this).parent().parent().find(\".right-div-comp-right-content\");\r\n" + 
	  		"    		}\r\n" + 
	  		"    	}\r\n" + 
	  		"    	//如果是工艺 则工艺和物料值\r\n" + 
	  		"    	if($(this).parent().attr(\"class\")=='right-div-comp-right-content-for-six'){\r\n" + 
	  		"    		dc = $(this).parent().parent().find(\".right-div-comp-right-content\");\r\n" + 
	  		"    	}\r\n" + 
	  		"    	//如果是材质 则材质和物料值\r\n" + 
	  		"    	if($(this).parent().attr(\"class\")=='right-div-comp-right-content-for-seven'){\r\n" + 
	  		"    		dc = $(this).parent().parent().find(\".right-div-comp-right-content\");\r\n" + 
	  		"    	}\r\n" + 
	  		"    	//如果是物料值 则物料值和文字\r\n" + 
	  		"    	if($(this).parent().attr(\"class\")=='right-div-comp-right-content'){\r\n" + 
	  		"    		dc = $(this).parent().parent().find(\".right-div-comp-left-text\");\r\n" + 
	  		"    	}\r\n" + 
	  		"    	\r\n" + 
	  		"    	if(dc==\"\"){console.info(\"err\")};\r\n" + 
	  		"    	//console.info($(dc).attr(\"class\"))\r\n" + 
	  		"    	var sum = dl.width() + dc.width();\r\n" + 
	  		"    	console.info(\"sum:\"+sum);\r\n" + 
	  		"    	var sum_father_width = $(this).parent().parent().parent().parent().width();\r\n" + 
	  		"    	//console.info(\"sum_father_width:\"+sum_father_width);\r\n" + 
	  		"        var me = $(this);\r\n" + 
	  		"        var deltaX = e.clientX ;\r\n" + 
	  		"               /* - \r\n" + 
	  		"                (parseFloat(me.position().left) || parseFloat(me.prop(\"clientLeft\")));*/\r\n" + 
	  		"        \r\n" + 
	  		"        /*console.info(me.prop(\"clientLeft\"));\r\n" + 
	  		"        console.info(me.position().left);*/\r\n" + 
	  		"        var _dlWidth=dl.width();\r\n" + 
	  		"        doc_mydraggablehandler.mousemove(function (e){\r\n" + 
	  		"        	//移动差值\r\n" + 
	  		"            var lt = e.clientX - deltaX; \r\n" + 
	  		"            console.info(\"移动差值:\"+lt);\r\n" + 
	  		"            /*lt = lt < 0 ? 0 : lt;\r\n" + 
	  		"            lt = lt > sum - me.width() ? sum - me.width() : lt;*/\r\n" + 
	  		"            \r\n" + 
	  		"            /*if(lt<0){\r\n" + 
	  		"            	//增大\r\n" + 
	  		"            	dc.width(dc.width()+lt);\r\n" + 
	  		"                dl.width(dl.width()-lt);\r\n" + 
	  		"                console.info(dc.width())\r\n" + 
	  		"                console.info(dl.width())\r\n" + 
	  		"            }\r\n" + 
	  		"            if(lt>0){\r\n" + 
	  		"            	//减小\r\n" + 
	  		"            	dc.width(dc.width()+lt);\r\n" + 
	  		"                dl.width(dl.width()-lt);\r\n" + 
	  		"                console.info(dc.width())\r\n" + 
	  		"                console.info(dl.width())\r\n" + 
	  		"            }*/\r\n" + 
	  		"        	//me.css (\"left\", lt);\r\n" + 
	  		"            //console.info(lt);\r\n" + 
	  		"            /*me.css (\"left\", lt);*/\r\n" + 
	  		"            console.info(\"移动量占比:\"+lt/sum_father_width*100+\"%\");\r\n" + 
	  		"            var rate = (_dlWidth-lt)/sum_father_width*100;\r\n" + 
	  		"            console.info(\"移动项总占比:\"+rate+\"%\");\r\n" + 
	  		"            if(rate<5){\r\n" + 
	  		"            	rate=5;\r\n" + 
	  		"            }\r\n" + 
	  		"            var total_rate=sum/(sum_father_width)*100;\r\n" + 
	  		"            console.info(\"移动关联两则在总面积占比:\"+total_rate+\"%\");\r\n" + 
	  		"            dl.width(rate+\"%\");\r\n" + 
	  		"            console.info(\"主动项占比\"+rate+\"%\");\r\n" + 
	  		"            dc.width((total_rate-rate)+\"%\");\r\n" + 
	  		"            console.info(\"关联项占比\"+(total_rate-rate)+\"%\");\r\n" + 
	  		"            console.info(\"mousemove\");\r\n" + 
	  		"            me.css (\"left\", dl.position().left);\r\n" + 
	  		"        });\r\n" + 
	  		"    });\r\n" + 
	  		"\r\n" + 
	  		"    $(doc_mydraggablehandler).mouseup (function(){\r\n" + 
	  		"    	doc_mydraggablehandler.unbind(\"mousemove\");\r\n" + 
	  		"    });\r\n" + 
	  		"    doc_mydraggablehandler.ondragstart \r\n" + 
	  		"    = doc_mydraggablehandler.onselectstart \r\n" + 
	  		"    = function () \r\n" + 
	  		"    {\r\n" + 
	  		"        return false;\r\n" + 
	  		"    };\r\n" + 
	  		"}\r\n" + 
	  		"/*url转化为base64*/\r\n" + 
	  		"//传入图片路径，返回base64\r\n" + 
	  		"function _mygetBase64(img){\r\n" + 
	  		"    function getBase64Image(img,width,height) {//width、height调用时传入具体像素值，控制大小 ,不传则默认图像大小\r\n" + 
	  		"      var canvas = document.createElement(\"canvas\");\r\n" + 
	  		"      canvas.width = width ? width : img.width;\r\n" + 
	  		"      canvas.height = height ? height : img.height;\r\n" + 
	  		"\r\n" + 
	  		"      var ctx = canvas.getContext(\"2d\");\r\n" + 
	  		"      ctx.drawImage(img, 0, 0, canvas.width, canvas.height);\r\n" + 
	  		"      var dataURL = canvas.toDataURL();\r\n" + 
	  		"      return dataURL;\r\n" + 
	  		"    }\r\n" + 
	  		"    var image = new Image();\r\n" + 
	  		"    image.crossOrigin = '';\r\n" + 
	  		"    image.src = img;\r\n" + 
	  		"    var deferred=$.Deferred();\r\n" + 
	  		"    if(img){\r\n" + 
	  		"      image.onload =function (){\r\n" + 
	  		"        deferred.resolve(getBase64Image(image));//将base64传给done上传处理\r\n" + 
	  		"      }\r\n" + 
	  		"      return deferred.promise();//问题要让onload完成后再return sessionStorage['imgTest']\r\n" + 
	  		"    }\r\n" + 
	  		"  }\r\n" + 
	  		"/*tinymce*/\r\n" + 
	  		"var Util_tinymce={\r\n" + 
	  		"	_this:\"\",\r\n" + 
	  		"	bindEvent:function(){\r\n" + 
	  		"		//表格\r\n" + 
	  		"		$(\"[name^='order_label_table']\").unbind(\"dblclick\");\r\n" + 
	  		"		$(\"[name^='order_label_table']\").bind(\"dblclick\",function(){\r\n" + 
	  		"			_this=this;\r\n" + 
	  		"			Util_tinymce.initTinyMce();\r\n" + 
	  		"		})\r\n" + 
	  		"		//自定义标签\r\n" + 
	  		"		$(\"[name^='order_label_label_val']\").unbind(\"dblclick\");\r\n" + 
	  		"		$(\"[name^='order_label_label_val']\").bind(\"dblclick\",function(){\r\n" + 
	  		"			_this=this;\r\n" + 
	  		"			Util_tinymce.initTinyMce();\r\n" + 
	  		"		})\r\n" + 
	  		"	},\r\n" + 
	  		"	initTinyMce:function(){\r\n" + 
	  		"		if($(_this)[0].tagName==\"DIV\"||$(_this)[0].tagName==\"TD\"){\r\n" + 
	  		"			$('div#easyui_dialog_index').dialog({\r\n" + 
	  		"	            modal: true,\r\n" + 
	  		"	            title: '富文本编辑',\r\n" + 
	  		"	            width: \"600\",\r\n" + 
	  		"	            height: \"400\",\r\n" + 
	  		"	            closed: false,\r\n" + 
	  		"	            border:true,\r\n" + 
	  		"	            cache: false,\r\n" + 
	  		"	            content:\"<iframe name='tinymce' frameborder='0' scrolling='no' style='width:99%;height:97%;' src='tinymce.html'></iframe>\",\r\n" + 
	  		"	            onClose : function() {  \r\n" + 
	  		"	               $(\"div#easyui_dialog_index\").dialog('destroy');  \r\n" + 
	  		"	               $(\"body\").append(\"<div id='easyui_dialog_index'></div>\");\r\n" + 
	  		"	            },\r\n" + 
	  		"	            buttons:[{\r\n" + 
	  		"					text:'确认',\r\n" + 
	  		"					handler:function(){\r\n" + 
	  		"						var content = tinymce.window.tinyMCE.activeEditor.getContent();\r\n" + 
	  		"						Util_tinymce._thisSetContent(content);\r\n" + 
	  		"						//关闭dialog\r\n" + 
	  		"						$(\"div#easyui_dialog_index\").dialog('destroy');  \r\n" + 
	  		"			            $(\"body\").append(\"<div id='easyui_dialog_index'></div>\");\r\n" + 
	  		"					}\r\n" + 
	  		"				}]\r\n" + 
	  		"	        });\r\n" + 
	  		"		}else{\r\n" + 
	  		"			alert($(_this)[0].tagName+\"不支持双击功能,请重新获取组件\");\r\n" + 
	  		"			return;\r\n" + 
	  		"		}\r\n" + 
	  		"	},\r\n" + 
	  		"	_thisGetContent:function(){\r\n" + 
	  		"		return $(_this).html();\r\n" + 
	  		"	},\r\n" + 
	  		"	_thisSetContent:function(content){\r\n" + 
	  		"		$(_this).html(content);\r\n" + 
	  		"	}\r\n" + 
	  		"}\r\n" + 
	  		"Util_tinymce.bindEvent();\r\n" + 
	  		"\r\n" + 
	  		"/**\r\n" + 
	  		" * 将base64变成blob\r\n" + 
	  		" * @param urlData\r\n" + 
	  		" * @returns\r\n" + 
	  		" */\r\n" + 
	  		"function Util_convertBase64UrlToBlob(urlData){  \r\n" + 
	  		"    var bytes=window.atob(urlData.split(',')[1]);        //去掉url的头，并转换为byte  \r\n" + 
	  		"    //处理异常,将ascii码小于0的转换为大于0  \r\n" + 
	  		"    var ab = new ArrayBuffer(bytes.length);  \r\n" + 
	  		"    var ia = new Uint8Array(ab);  \r\n" + 
	  		"    for (var i = 0; i < bytes.length; i++) {  \r\n" + 
	  		"        ia[i] = bytes.charCodeAt(i);  \r\n" + 
	  		"    }  \r\n" + 
	  		"    return new Blob( [ab] , {type : 'image/png'});  \r\n" + 
	  		"}\r\n" + 
	  		"/*imgToBlob*/\r\n" + 
	  		"var Util_loadImageToBlob  = function(url, callback) {\r\n" + 
	  		"	if(!url || !callback) return false;\r\n" + 
	  		"	var xhr = new XMLHttpRequest();\r\n" + 
	  		"	xhr.open('get', url, true);\r\n" + 
	  		"	xhr.responseType = 'blob';\r\n" + 
	  		"	xhr.onload = function() {\r\n" + 
	  		"		// 注意这里的this.response 是一个blob对象 就是文件对象\r\n" + 
	  		"		callback(this.status == 200 ? this.response : false);\r\n" + 
	  		"	}\r\n" + 
	  		"	xhr.send();\r\n" + 
	  		"	return true;\r\n" + 
	  		"}\r\n" + 
	  		"/**\r\n" + 
	  		" * 将blob转换为url\r\n" + 
	  		" * window.URL.createObjectURL(blobdata)\r\n" + 
	  		" */\r\n" + 
	  		"/**\r\n" + 
	  		" * window.parent.Util_loadImageToBlob(json.data[0], function(blobFile) {//这里的img.src改地址\r\n" + 
	  		"			if(!blobFile) return false;\r\n" + 
	  		"//			var fileReader = new FileReader();\r\n" + 
	  		"//			fileReader.readAsDataURL(blobFile);\r\n" + 
	  		"//			fileReader.onload = function() {\r\n" + 
	  		"//				console.log(this.result);//这里输出的数据放到url里能生成图片\r\n" + 
	  		"//			};\r\n" + 
	  		"			console.info(window.URL.createObjectURL(blobFile));\r\n" + 
	  		"			success(window.URL.createObjectURL(blobFile));\r\n" + 
	  		"		  });\r\n" + 
	  		"*/		\r\n" + 
	  		"//绑定工艺事件\r\n" + 
	  		"var Util_artsEvent={\r\n" + 
	  		"	_this:\"\",\r\n" + 
	  		"	material_type:\"\",\r\n" + 
	  		"	tagName:\"\",\r\n" + 
	  		"	bindEvent:function(){\r\n" + 
	  		"		$(\"[name='order_s_arts']\").unbind(\"click\");\r\n" + 
	  		"		$(\"[name='order_s_arts']\").bind(\"click\",function(){\r\n" + 
	  		"			Util_artsEvent._this=this;\r\n" + 
	  		"			Util_artsEvent.tagName=\"DIV\";\r\n" + 
	  		"			var uniqueid = $(this).parent().attr(\"uniqueid\")\r\n" + 
	  		"			Util_artsEvent.material_type=$('[uniqueid=\"'+uniqueid+'\"]').find('[name=\"order_s_material_type\"]').text();\r\n" + 
	  		"			Util_artsEvent.showDialog();\r\n" + 
	  		"		});\r\n" + 
	  		"		$(\"[name='order_s_input_val_arts']\").unbind(\"click\");\r\n" + 
	  		"		$(\"[name='order_s_input_val_arts']\").bind(\"click\",function(){\r\n" + 
	  		"			Util_artsEvent._this=this;\r\n" + 
	  		"			Util_artsEvent.tagName=\"TEXTAREA\";\r\n" + 
	  		"			Util_artsEvent.material_type=$(this).parent().parent().find(\"[name='order_s_input_material_type']\").text();\r\n" + 
	  		"			Util_artsEvent.showDialog();\r\n" + 
	  		"		});\r\n" + 
	  		"	},\r\n" + 
	  		"	showDialog:function(){\r\n" + 
	  		"		$('div#easyui_dialog_index').dialog({\r\n" + 
	  		"            modal: true,\r\n" + 
	  		"            title: '工艺选择',\r\n" + 
	  		"            width: \"400\",\r\n" + 
	  		"            height: \"300\",\r\n" + 
	  		"            closed: false,\r\n" + 
	  		"            border:true,\r\n" + 
	  		"            cache: false,\r\n" + 
	  		"            content:\"<iframe name='tinymce' frameborder='0' scrolling='no' style='width:99%;height:97%;' src='arts_dialog.html'></iframe>\",\r\n" + 
	  		"            onClose : function() {  \r\n" + 
	  		"               $(\"div#easyui_dialog_index\").dialog('destroy');  \r\n" + 
	  		"               $(\"body\").append(\"<div id='easyui_dialog_index'></div>\");\r\n" + 
	  		"            }\r\n" + 
	  		"        });\r\n" + 
	  		"	},\r\n" + 
	  		"	setContent:function(content){\r\n" + 
	  		"		console.info(content);\r\n" + 
	  		"		if(this.tagName=\"DIV\"){\r\n" + 
	  		"			$(this._this).text(content);\r\n" + 
	  		"		}\r\n" + 
	  		"		if(this.tagName=\"TEXTAREA\"){\r\n" + 
	  		"			$(this._this).val(content);\r\n" + 
	  		"		}\r\n" + 
	  		"		//关闭dialog\r\n" + 
	  		"		$(\"div#easyui_dialog_index\").dialog('destroy');  \r\n" + 
	  		"        $(\"body\").append(\"<div id='easyui_dialog_index'></div>\");\r\n" + 
	  		"	}\r\n" + 
	  		"}		  \r\n" + 
	  		"		  \r\n" + 
	  		"\r\n" + 
	  		"\r\n" + 
	  		""));
  }
}