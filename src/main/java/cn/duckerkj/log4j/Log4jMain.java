package cn.duckerkj.log4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import cn.duckerkj.exception.ExceptionUtils;

/**
 * log4j功能测试
 * @author Shensg
 * 2018年7月30日
 */
public class Log4jMain {
	public   static   void  main(String[] args)   {
        PropertyConfigurator.configure( "C:\\Users\\Administrator\\git\\JavaTools\\src\\main\\java\\log4j.properties" );
        Logger logger  =  Logger.getLogger(Log4jMain. class );
        logger.debug( " debug " );
        logger.error( " error " );
        logger.info( " info " );
        try {
			throw new Exception("无聊,报个错！");
		} catch (Exception e) {
			logger.error(ExceptionUtils.getStackTrace(e));
		}
    } 
}
