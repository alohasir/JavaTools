package cn.duckerkj.deepclone;
/**
 * 深度克隆 序列化方式
 * 其余方式
 * 	1、clone 如果对象会引用相同地址是浅克隆，导致每个类都需要重写clone并且需要写深克隆方法 ，当类很多的时候麻烦
 * 	2、利用反射机制
 * 	3、序列化 只要类上实现Serialzable 最简便方式使用Apache下commons 的SerialzationUtils类
 * @author Shensg
 * 2018年7月13日
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class CloneUtils {
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T clone(T obj) {
		//拷贝产生的对象
		T cloneObj = null;
		try {
			//读取对象字节数据
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(obj);
			oos.close();
			// 分配内存空间,写入原始对象,生成新对象
			ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bis);
			//返回新对象并做类型转换
			cloneObj = (T)ois.readObject();
			ois.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return cloneObj;
		
	}
}
