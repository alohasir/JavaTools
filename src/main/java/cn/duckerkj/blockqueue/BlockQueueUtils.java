package cn.duckerkj.blockqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 阻塞式队列
 * 
 * 相当于指定人数等待
 * @author Shensg
 * 2018年7月17日
 */
public class BlockQueueUtils {
	public static void main(String[] args) throws InterruptedException {
		BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(5);
		//ArrayList 可以自动扩容
		//Error BlockQueue Queue full
		for (int i = 0; i < 10; i++) {
			//blockingQueue.add("s"+i);//Error BlockQueue Queue full
			blockingQueue.offer("s"+i);
			blockingQueue.offer("s"+i,	10, TimeUnit.HOURS);
			blockingQueue.put("s"+i);
		}
	}
}
