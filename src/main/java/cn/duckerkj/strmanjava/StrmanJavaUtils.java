package cn.duckerkj.strmanjava;

import strman.Strman;

/**
 * 字符串工具
 * @author shensg
 *
 <dependency>
    <groupId>com.shekhargulati</groupId>
    <artifactId>strman</artifactId>
    <version>0.4.0</version>
</dependency>
 
 
 
 
 */
public class StrmanJavaUtils {
	
	public static void main(String[] args) {
		//字符串拼接
		System.out.println("字符串拼接 ==>"+Strman.append("foo", "ba","r"));
		//字符串数组拼接
		System.out.println("字符串拼接数组==>"+Strman.appendArray("f", new String[]{"o", "o", "b", "a", "r"}));
		//获取某个下标下的值
		System.out.println("获取字符串下标的值==>"+Strman.at("foobar", 0).get());
		//多个数组整合成为一个数组
		System.out.println("合并数组==>"+Strman.between("[abc][def][][]", "[", "]").length);
		//字符串变数组
		System.out.println("字符串变成数组==>"+Strman.chars("abcdef").length);
		//统计字符串某个字符的数量 
		System.out.println("统计字符串某个字符的数量==>"+Strman.charsCount("abca").get('a'));//get的时候用char类型
		//合并多个空格变成一个空格
		System.out.println("合并多个空格变成一个空格==>"+Strman.collapseWhitespace("a     c"));
		//清空所有空格
		System.out.println("清空所有空格==>"+Strman.removeSpaces("   123 abc    "));
		//判断字符中是否包含一个字符
		System.out.println("判断字符中是否包含另一个字符不区分大小写==>"+Strman.contains("foo bar","Foo"));//不区分大小写
		System.out.println("判断字符中是否包含另一个字符区分大小写==>"+Strman.contains("foo bar","Foo",true));//区分大小写
		//判断字符串是否包含多个字符
		System.out.println("判断字符中是否包含多个字符不区分大小写==>"+Strman.containsAll("foo bar", new String[]{"Foo", "bar"}));//不区分大小写 默认不区分
		System.out.println("判断字符中是否包含多个字符区分大小写==>"+Strman.containsAll("foo bar", new String[]{"Foo", "bar"},true));//区分大小写
		//判断字符串是否包含多个字符其中一个
		System.out.println("判断字符串是否包含多个字符其中一个不区分大小写==>"+Strman.containsAny("bar foo", new String[]{"FOO", "BAR", "Test"}, false));
		//统计按照某个分割之后数量
		System.out.println("统计按照某个分割之后数量 区分大小写并且重叠==>"+Strman.countSubstr("aaaAAAaaa", "aaa",true,true));
		System.out.println("统计按照某个分割之后数量 不区分大小写并且不重叠==>"+Strman.countSubstr("aaaAAAaaa", "aaa",false,false));
		//判断以某个字符串结尾
		//endsWith("foo Bar", "BAR", false) 
		//以某个字符串为前缀
		System.out.println("以某个字符串为前缀,如果不存在追加，区分大小写==>"+Strman.ensureLeft("foobar", "FOO", true));
		//以某个字符串作为后缀
		System.out.println("以某个字符串为后缀,如果不存在追加，区分大小写==>"+Strman.ensureRight("foobar", "BAR", true));
		//字段截取从第一个开始到n
		System.out.println("字段截取从第一个开始到n==>"+Strman.first("foobar", 3));
		//字段截取从最后开始到n
		System.out.println("字段截取从最后一个开始到n==>"+Strman.last("foobar", 3));
		//取出字符串第一个字符
		System.out.println("取出字符串第一个字符==>"+Strman.head("foobar"));
		//tail("foobar")
		// result => "oobar"
		
		//inequal  ==  !equal
		//字符串插入 insert("fbar", "oo", 1)
		//左补全
		System.out.println("左补全==>"+Strman.leftPad("1", "0", 5));
		//右补全
		//rightPad
		//去左边空格
		System.out.println("去除左边空格==>"+Strman.leftTrim("  1  a  "));
		//去右边空格
		//rightTrim
		//清除数组中空的值
		System.out.println("清除数组中空的值==>"+Strman.removeEmptyStrings(new String[]{"aa", "", "   ", "bb", "cc", null}));
		//移除第一个符合的字符串 区分大小写
		System.out.println("移除第一个符合的字符串 不区分大小写==>"+Strman.removeLeft("FOOfooAfoo", "foo",false));
		System.out.println("移除第一个符合的字符串 区分大小写==>"+Strman.removeRight("FOOfooAfoo", "foo",true));
		//清空不是字母的值 a-z0-9
		System.out.println("清空不是字母的值==>"+Strman.removeNonWords("qwe123-=\\/1中国/"));
		//重复repeat
		//repeat("1", 3)
			// result  => "111"
		//反转字符串
		//reverse("foo")
			// result => "oof"
		//字符太长变成其他字符代替 推荐safeTruncate
		System.out.println("字符过长自动转换为==>"+Strman.safeTruncate("A Javascript string manipulation library.", 16, "..."));
		//truncate("A Javascript string manipulation library.", 14, "...")
		// result => "A Javascrip..."
		
		//字符串随机拼接
		System.out.println(Strman.shuffle("shekhar"));
		//slugify("foo bar")
		// result => "foo-bar"
		
		//移除英标
		//transliterate("fóõ bár")
		// result => "foo bar"
		
		//包围字符串
		System.out.println("包围字符串==>"+Strman.surround("div", "<", ">"));
		
		//驼峰表达式转换
		//toCamelCase("CamelCase")
		// result => "camelCase"
		//toCamelCase("camel-case")
		// result => "camelCase"
		
		
		//toStudlyCase("hello world")
		// result => "HelloWorld"
		//System.out.println("首字母大写==>"+Strman.toStudlyCase("hello world cc"));
		//toDecamelize("helloWorld",null)
		// result => "hello world"
		//toKebabCase("hello World")
		// result => "hello-world"
		//toSnakeCase("hello world")
		// result => "hello_world"
		//首字母大写
		//upperFirst("fred")
		// result => "Fred"
		
		//words("This is a string, with words!")
		// result => ["This", "is", "a", "string", "with", "words"]
		
		
		//isEnclosedBetween("{{shekhar}}", "{{", "}}")
		// result => true
		
		//join(new String[]{"hello","world","123"}, ";")
		// result => "hello;world;123")
		
		//判断是否为空
		//isBlank("")
		// result => true)
		//isBlank(null)
		// result => true)
		//isBlank("test")
		// result => false)
		
		//下划线
		//underscored("MozTransform")
		// result => "moz_transform")
		
		//
		//zip("abc", "def")
		// result => ["ad", "be", "cf"]
		//zip("abc", "d")
		// result => ["ad"]
		
		//一段话一个
		//lines("Hello\r\nWorld")
		// result => ["Hello", "World"]
		
		//dasherize("the_dasherize_string_method")
		// result => "the-dasherize-string-method"
		
		//humanize("the_humanize_method")
		// result => "The humanize method"
		
		
		//大小写转换
		//swapCase("AaBbCcDdEe")
		// result => "aAbBcCdDeE"
		
		//按量分割
		//chop("whitespace", 3);
		// result => ["whi", "tes", "pac", "e"]
		
		//base64编码
		//base64Encode("strman")
		//base64解码
		//base64Decode("c3RybWFu")
		//二进制编码
		//binEncode("A");
		//二进制解码
		//binDecode
		//十进制编码
		//decEncode
		//十进制解码
		//decDecode
		//十六进制编码
		//hexEncode
		//十六进制解码
		//hexDecode
		//html编码解码
		//htmlDecode
		
		
	}
}
