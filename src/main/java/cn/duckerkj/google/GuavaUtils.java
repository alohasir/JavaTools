package cn.duckerkj.google;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
/**
 * 缓存问题
 * @author Shensg
 * 2018年8月1日
 */
public class GuavaUtils {
	public static Cache<String, List<String>> cache = CacheBuilder.newBuilder()  
            .expireAfterAccess(60, TimeUnit.SECONDS).maximumSize(1000)  
            .build();  
	public static void main(String[] args) {
		
		try {
			List<String> list = cache.get("points", new Callable<List<String>>() {

				public List<String> call() throws Exception {
					// TODO Auto-generated method stub
					List<String> ret = new ArrayList<String>();
					ret.add("1");
					return ret;
				}
			});
			for(String str:list) {
				System.out.println(str);
			}
			
			List<String> list2 = cache.get("points", new Callable<List<String>>() {

				public List<String> call() throws Exception {
					// TODO Auto-generated method stub
					List<String> ret = new ArrayList<String>();
					ret.add("123456");
					return ret;
				}
			});
			for(String str:list2) {
				System.out.println(str);
			}
			try {  
		        Thread.currentThread();  
		        Thread.sleep(TimeUnit.SECONDS.toMillis(10));  
		    } catch (InterruptedException e) {  
		        // TODO Auto-generated catch block  
		        e.printStackTrace();  
		    }
			List<String> list3 = cache.get("points", new Callable<List<String>>() {

				public List<String> call() throws Exception {
					// TODO Auto-generated method stub
					List<String> ret = new ArrayList<String>();
					ret.add("123456789");
					return ret;
				}
			});
			for(String str:list3) {
				System.out.println(str);
			}
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
