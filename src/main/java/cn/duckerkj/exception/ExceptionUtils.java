package cn.duckerkj.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 
 * @author Shensg
 * 2018年7月31日
 */
public class ExceptionUtils {
	public static String getStackTrace(Throwable throwable)
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        try
        {
            throwable.printStackTrace(pw);
            return sw.toString();
        } finally
        {
            pw.close();
        }
    }
}
