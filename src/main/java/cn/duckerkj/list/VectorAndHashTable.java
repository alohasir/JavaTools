package cn.duckerkj.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * 多线程并且线程安全操作
 * @author Shensg
 * 2018年7月13日
 */
public class VectorAndHashTable {
	public static void main(String[] args) {
		//火车票列表
		final List<String> tickets = new Vector<String>();//ArrayList 线程不安全
		//初始化票据池
		for(int i=0;i<1000;i++) {
			tickets.add("ticket"+i);
		}
		//退票
		/*Thread returnThread = new Thread() {
			public void run() {
				while(true) {
					tickets.add("ticket"+new Random().nextInt());
				}
			}
		};*/
		//购票
		for (int i = 0; i < 30; i++) {
			new Thread() {
				public void run() {
					while(true) {
						System.out.println(Thread.currentThread().getId()+"----"+tickets.remove(0));
					}
				}
			}.start();
		}
		//returnThread.start();
		//saleThread.start();
	}
}
