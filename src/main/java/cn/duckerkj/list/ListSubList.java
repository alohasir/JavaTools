package cn.duckerkj.list;

import java.util.ArrayList;
import java.util.List;

/**
 * subList 类似视图
 * 但是不能对原list操作会报错。只能对sublist操作
 * @author Shensg
 * 2018年7月13日
 */
public class ListSubList {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("1");
		list.add("2");
		list.add("3");
		System.out.println(list);
		List<String> sublist = list.subList(0, 1);
		System.out.println(list);
		sublist.clear();
		sublist.add("4");
		System.out.println(list);
		System.out.println(sublist);
	}
}
