package cn.duckerkj.thread;
/**
 * 线程自动重启
 * 
 * 注意问题
 * 1、共享资源锁定 , 因为异常被锁定导致系统负担大
 * 2、原子操作
 * 3、反复重启避免内存溢出 可以设置重启次数
 * @author Shensg
 * 2018年7月16日
 */
public class ThreadUtils implements Runnable{
	public ThreadUtils() {
		Thread thread = new Thread(this);
		thread.setUncaughtExceptionHandler(new ThreadUtilsExceptionHandler());
		thread.start();
	}

	public void run() {
		// TODO Auto-generated method stub
		//我的业务逻辑
		for(int i=0;i<3;i++) {
			System.out.println("我的业务正常执行中:"+i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//啊！执行出错了,看看有没有自动启动
		throw new RuntimeException();
	}
	private class ThreadUtilsExceptionHandler implements Thread.UncaughtExceptionHandler{

		public void uncaughtException(Thread t, Throwable e) {
			// TODO Auto-generated method stub
			System.out.println("线程:"+t.getName()+"出现问题,请分析原因");
			e.printStackTrace();
			//重启业务不中断
			new ThreadUtils();
		}
	}
	public static void main(String[] args) {
		new ThreadUtils();
	}
}
