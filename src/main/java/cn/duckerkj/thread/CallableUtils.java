package cn.duckerkj.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 获取线程返回处理结果
 * 优点
 * 	尽可能多地占用资源
 * 	可以监控是否完成
 * 	可以给用户提供进度
 * @author Shensg
 * 2018年7月16日
 */
public class CallableUtils implements Callable<Integer>{

	//本金
	private int seedMoney;
	
	public CallableUtils(int seedMoney) {
		super();
		this.seedMoney = seedMoney;
	}

	public Integer call() throws Exception {
		// TODO Auto-generated method stub
		//模拟复杂计算需要10s
		TimeUnit.MILLISECONDS.sleep(10000);
		return seedMoney/10;
	}
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//生成一个单线程异步处理器 
		ExecutorService es = Executors.newSingleThreadExecutor();
		//线程执行完之后的期望值
		Future<Integer> future = es.submit(new CallableUtils(500));
		while(!future.isDone()) {
			//还没有执行完毕等待200 ms
			TimeUnit.MILLISECONDS.sleep(200);
			System.out.print("#");
		}
		System.out.println("期望结果"+future.get());
		es.shutdown();
	}

}
