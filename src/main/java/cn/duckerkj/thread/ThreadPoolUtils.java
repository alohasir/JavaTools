package cn.duckerkj.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 多线程
 * @author Shensg
 * 2018年7月16日
 */
public class ThreadPoolUtils {
	public static void main(String[] args) {
		//创建10个线程的线程池
		ExecutorService es = Executors.newFixedThreadPool(10);
		/* 单个线程
		 * Executors.newSingleThreadExecutor();*/
		/* 缓冲
		 * Executors.newCachedThreadPool();*/
		//没有返回值
		for(int i=0;i<20;i++) {
			es.submit(new Runnable() {
				
				public void run() {
					// TODO Auto-generated method stub
					System.out.println(Thread.currentThread().getName());
				}
			});
		}
		//有返回值
		for(int i=0;i<30;i++) {
			Future<Integer> future = es.submit(new CallableUtils(500));
			System.out.println("#"+i);
		}
		
		es.shutdown();
	}
}
