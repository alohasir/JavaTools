package cn.duckerkj.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
/**
 * JedisPool
 * @author Shensg
 * 2018年7月26日
 */
public class RedisCache {
	 
	
    public RedisCache()  {
		throw new RuntimeException("此类不允许被new");
	}

	public static JedisPool jedisPool ;

    /**
     * 构建redis连接池
     * 
     * @param ip
     * @param port
     * @return JedisPool
     */
    public static JedisPool getPool() {
        if (jedisPool == null) {
            JedisPoolConfig config = new JedisPoolConfig();
            //控制一个pool可分配多少个jedis实例，通过pool.getResource()来获取；
            //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
            config.setMaxIdle(500);
            //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
            config.setMaxIdle(5);
            //表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
            config.setMaxWaitMillis(1000 * 30);
            //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
            config.setTestOnBorrow(true);
            String host = "127.0.0.1";
            String password = "redis";
            jedisPool =new JedisPool(config, host, 6379, 3000, password);
        }
        return jedisPool;
    }
    
    /**
     * 返还到连接池
     * 
     * @param pool 
     * @param redis
     */
    public static void returnResource(JedisPool pool, Jedis redis) {
        if (redis != null) {
            redis.close();
        }
    }
    
    
    //从redis缓存中查询，反序列化
    public static Object getDataFromRedis(String redisKey){
    	getPool();
    	
        //查询
        Jedis jedis = jedisPool.getResource();
        byte[] result = jedis.get(redisKey.getBytes());

        //如果查询没有为空
        if(null == result){
        	returnResource(jedisPool,jedis);
            return null;
        }
        returnResource(jedisPool,jedis);
        //查询到了，反序列化
        return SerializeUtil.unSerialize(result);
    }

    //将数据库中查询到的数据放入redis
    public static void setDataToRedis(String redisKey, Object obj ,String expiretime){
    	getPool();
        //序列化
        byte[] bytes = SerializeUtil.serialize(obj);

        //存入redis
        Jedis jedis = jedisPool.getResource();
        String success = jedis.set(redisKey.getBytes(), bytes);
        jedis.expire(redisKey.getBytes(), Integer.valueOf(expiretime));
        if("OK".equals(success)){
            System.out.println("数据成功保存到redis...");
        }
        returnResource(jedisPool,jedis);
    }
    
    public static void main(String[] args) {
		setDataToRedis("ssg", "123", "10");
		System.out.println(getDataFromRedis("ssg"));
	}
    
}